package slackapi

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
)

func SendSlackMessage(token string, channelName string, msg string, slackDomainName string) {

	slackUrl := "https://" + slackDomainName + ".slack.com/services/hooks/slackbot?token=" + token + "&channel=%23" + channelName

	log.Println("SendSlackMessage URL=" + slackUrl + " message=" + msg)

	msgBuf := bytes.NewBufferString(msg)
	resp, err := http.Post(slackUrl, "text/plain", msgBuf)
	if err != nil {
		log.Println(err)
		panic(err)
	}
	defer resp.Body.Close()
}

func SendSlackMessageToIsYourWebsiteDown(msg string) {
	slackIncomingWebhookUrl := "https://hooks.slack.com/services/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"

	values := map[string]string{
		"text": msg,
	}

	jsonValue, _ := json.Marshal(values)

	resp, err := http.Post(slackIncomingWebhookUrl, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		log.Printf("ERROR SendSlackMessageToIsYourWebsiteDown: %v\n", err)
	}
	defer resp.Body.Close()
}

func SendSlackMessageToResetHabit(msg string) {
	slackIncomingWebhookUrl := "https://hooks.slack.com/services/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"

	values := map[string]string{
		"text": msg,
	}

	jsonValue, _ := json.Marshal(values)

	resp, err := http.Post(slackIncomingWebhookUrl, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		log.Printf("ERROR SendSlackMessageToResetHabit: %v\n", err)
	}
	defer resp.Body.Close()
}
